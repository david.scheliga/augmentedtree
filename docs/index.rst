augmentedtree
=============

.. augmentedtree documentation master file, created by
   sphinx-quickstart on Thu Jan 17 21:49:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: resources/augmentedtree-icon.png
   :height: 192px
   :width: 192px
   :alt: Circle, square, triangle and lines.
   :align: center


`augmentedtree` enhances mappings and sequences (targeted for python
dictionaries and lists) preserving their native behavior and access.
The enhancement comes with getting values by single keys, human readable
viewing, selecting and setting multiple values/items within the nested
data at once, *or*-conditional selection of values/items. Also this package
intends to prepare the nested data for pyQt. The augmentation provides
methods and properties to be used for a `QAbstractItemModel` showing
the nested data within a `QTreeView`.

Installation
------------

Installing the latest release using pip is recommended.

::

   $ pip install augmentedtree

The latest development state can be obtained from gitlab using pip.

::

   $ pip install git+https://gitlab.com/david.scheliga/augmentedtree.git@dev


Purpose of *augmentedtree*
--------------------------

The main purpose of this package is enhance nested data structure (mostly nested
dictionaries) by keeping its functionality for methods working with these data types.

The targeted usage is to be able to write the following kind of code

.. code-block:: python

   # code where the nested data comes from
   ...

   # gathering parameters
   with AugmentedTree(nested_data) as tree:
      # simple selection
      first_value = tree.select("something", "here")[0]
      last_value = tree.select("something", "there")[-1]
      a_slice_of_values = tree.select("a", "l?t", "of")[3:6]

      # selection with refinement
      selection_of_values = tree.select("also/a", "lot", "of")
      narrowed_down = selection_of_values.where("this", "or", "that")[ALL_ITEMS]
       ...

   if not tree.all_selections_succeeded:
       # break, exit or reacting to some value are not there
       ...

   # code which is working with requested parameters
   ...

Limitations
-----------

The augmented tree items are directly forwarding the nested data. Practically objects
implementing collections.abc.Sequence and collection.abc.Mapping should work with this
package. Nevertheless:

- This package was developed with using `list` and `dict`. Other classes weren't tested
  yet.
- Keep in mind that inserting new items directly into the nested data will mess up
  the augmentation. If `AnAugmentedTreeItem` should keep track of new items usage of
  this layer is mandatory.

The **development status** is **alpha**.

- Changes will come.
- Definition of types for fields in MappingTreeItem will be implemented.
- **Selection** items will be reworked. (SPARQL support is a desirable option.)


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   basic_usage
   detailed_examples
   an_augmented_tree_item
   package/package
