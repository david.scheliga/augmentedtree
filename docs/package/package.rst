package
=======

Augmented tree items
--------------------

Abstract Base Classes
+++++++++++++++++++++

.. py:currentmodule:: augmentedtree

.. autoclass:: AnAugmentedTreeItem
   :show-inheritance:

.. autoclass:: AnAugmentedCollection
   :show-inheritance:

Base tree item classes
++++++++++++++++++++++

.. autoclass:: ATreeItem
   :show-inheritance:
   :members: primekey, primename, primevalue, real_key, children,
             has_primekey

.. autoclass:: ACollectionTreeItem
   :show-inheritance:
   :members: outervalues

Tree item classes
+++++++++++++++++

.. autoclass:: ValueTreeItem
   :show-inheritance:

.. autoclass:: SequenceTreeItem
   :show-inheritance:

.. autoclass:: MappingTreeItem
   :show-inheritance:

.. autoclass:: AugmentedTree
   :show-inheritance:

.. autofunction:: augment_datastructure

.. autofunction:: print_atree

.. autoclass:: LeafType
   :members:

Selecting values
----------------

By default the methods :func:`select` and :func:`where` of :class:`AugmentedTree` and
:class:`AugmentedItemSelection` interprets any given path component as an UNIX
file pattern. To use regular expressions instead, these path components can be wrapped
with :class:`RegularExpressionPart`.

.. autoclass:: RegularExpressionPart

.. autoclass:: AugmentedItemSelection
   :show-inheritance:

Enhancement of Mappings by *schemas*
------------------------------------

.. autofunction:: use_mappingtree_schema
.. autofunction:: use_mappingtree_schemas


.. autoclass:: MappingSchema

    .. autoattribute:: MappingSchema.PRIMARYKEY
    .. autoattribute:: MappingSchema.PRIMARYNAME
    .. autoattribute:: MappingSchema.OUTERVALUES
    .. autoattribute:: MappingSchema.METAFIELDKEYS
    .. autoattribute:: MappingSchema.IDENTIFIER
    .. autoattribute:: MappingSchema.META_ATTRIBUTES

.. autoclass:: MappingSchemaBuilder
    :members: construct, construct_from_collection

Tree path related
-----------------

.. autoclass:: augmentedtree.core.TreePath
   :members:

.. autofunction:: augmentedtree.core.normalize_path_of_tree

.. testsetup:: *

   from augmentedtree.core import normalize_path_of_tree

.. doctest::

   >>> normalize_path_of_tree("//to/many/delimiters///everywhere//")
   '/to/many/delimiters/everywhere'
   >>> normalize_path_of_tree("missing/front/delimiter")
   '/missing/front/delimiter'
   >>> normalize_path_of_tree("/this/path/is/correct")
   '/this/path/is/correct'
   >>> normalize_path_of_tree("/unwanted/delimiter/at/the/end/")
   '/unwanted/delimiter/at/the/end'
   >>> normalize_path_of_tree(None)
   ''
   >>> normalize_path_of_tree([])
   ''

.. doctest::

   >>> normalize_path_of_tree("//to", "/many/delimiters//", "/everywhere//")
   '/to/many/delimiters/everywhere'
   >>> normalize_path_of_tree("missing", "front/delimiter")
   '/missing/front/delimiter'
   >>> normalize_path_of_tree("this", "path", "is/correct")
   '/this/path/is/correct'
   >>> normalize_path_of_tree("unwanted", "delimiter", "at/the/end/")
   '/unwanted/delimiter/at/the/end'
   >>> normalize_path_of_tree("invalid", "type", [], "within/the/path")
   '/invalid/type/within/the/path'
   >>> normalize_path_of_tree(None)
   ''
   >>> normalize_path_of_tree([])
   ''