# Changelog
This changelog is inspired by [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Release 0.2a0 (25.05.2020)
### Added
- sort-method for AugmentedTree and AugmentedTreeSelection
- *augmentedtree.MappingTreeItem* now supports accessing items using their key name as
  attribute.
- *MappingTreeItem* now supports *copy.copy* and *copy.deepcopy* and have equally named
  method *copy* and *deepcopy*.
- *MappingTreeItem* now takes the keyword *augmented_classes* to define
  its origin class augmentation schema.
- Using a path delimiter with a trailing single asterisk wildcard "<anything>/*" returns
  only items at that level.

### Changed
- setup.py doesn't clean up the directory anymore. It has to be transferred into a
  shell script.
- module *augmentedtree.abc* is renamed to *augmentedtree.abstractbaseclasses* due to
  interfering with the *abc* package.

## Release 0.1a0 (05.03.2020) - The selection rework.
### Added
- The new class *PathMap* provides the *select* and *where* methods, which are
  used and redirected by *AugmentedTree* and *TreeItemSelection*
- *AugmentedTree* and *TreeItemSelection* now have a *pathmap*-property
- It is now possible to search for 'name is value' like conditions within the *where*
  method by supplying a 'name/value' search part.
- Via *iter_selection* AugmentedTree and TreeItemSelection iterates trough their tree
  items, returning each tree item's *primekey* and *primevalue*, which are in case
  of an mapping the key, value pair and in case of a sequence the index and value. 

### Fixed
- Multiple connected question mark wildcard (unix file pattern) are now implemented 
  correctly.
- Multiple search path parts as an or-condition now work properly, when used as the 
  last part of *select*
- *where* selections now consider or-conditions correctly.

### Changed
- *select* and *where* were reworked to work properly.
- A empty TreeItemSelection now returns an empty list if it is sliced by [:] equal
  to [ALL_ITEMS] instead of None. This is a more expected result.
- Everything considered to be related with the selection of tree items is put into
  new module *treeitemselection*.
- Unix file pattern star '*' wildcards now support a better and precise *selection* of
  tree items. Read the docs at [Getting all unknown items of a specific item](https://augmentedtree.readthedocs.io/en/latest/basic_usage.html#Selecting-using-UNIX-wildcards#Getting-all-unknown-items-of-a-specific-item)
- Meta_attributes of items are now tuples of the meta attributes name and value to
  be able to search for *name is value* queries. Prior to this only values were 
  considered.
- The *treeitem* attribute of *AugmentedTree* and *TreeItemSelection* now supports
  slicing.
- Changed keys within the nested example data in the chapter *basic usage' of the
  documentation.

### Deprecated
- *UnixFilePatternPart*, *RegularExpressionPart* will be replaced by
  *UnixFilePatternParts*, *RegularExpressionParts* since they can hold one to many
  tree path parts.

## Release 0.0a2 (2020.02.07)
### Added
- Added *normalize_path_of_tree* to documentation.
- Added doctest to package.rst for *normalize_path_of_tree*.

### Changed
- Made the setup.py more convenient by adding cleaning directory, since
  *clean* argument didn't work with setuptools.

### Fixed
- *augmentedtree.core.normalize_path_of_tree* ignores other data types than
  *str*, *int* or *float* within the path creation.

## Release 0.0a1.post4 (2020.02.03)
### Added
- Added changelog.
- Added source code and documentation link to setup.

### Fixed
- Fixed broken link.

## Release 0.0a1.post3 (2020.02.02)

### Fixed
- Fixed all wrong links in the docs and readme.md.
- Fixed missing packages within requirements.txt

### Added
- All tree item classes are now shown within the package chapter.

## Release 0.0a1 (2020.02.02)
First satisfying alpha state of project.